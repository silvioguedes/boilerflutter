import 'package:boiler_flutter/features/main_tecnico/main_tecnico_page.dart';
import 'package:flutter/material.dart';
import 'package:boiler_flutter/shared/utils/utils.dart';
import 'package:boiler_flutter/localizations/localizations.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  final emailController = TextEditingController();
  final passwordController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        decoration: new BoxDecoration(
          image: new DecorationImage(
            image: getBackgroundImage(),
            fit: BoxFit.cover,
          ),
        ),
        child: Row(
          children: <Widget>[
            Expanded(
              flex: 1,
              child: Container()
            ),
            Expanded(
              flex: 8,
              child: FractionallySizedBox(
                widthFactor: 1.0,
                heightFactor: getHeightFactor(),
                child: Container(
                  decoration: BoxDecoration(
                    color: Colors.deepPurple
                  ),
                  child: Padding(
                    padding: EdgeInsets.only(
                      left: 30,
                      right: 30,
                      top: 0,
                      bottom: 0
                    ),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        TextField(
                          controller: emailController,
                          decoration: InputDecoration(
                              hintText: DemoLocalizations.of(context).email,
                              fillColor: Colors.white,
                              filled: true
                          ),
                          cursorColor: Colors.black,
                          keyboardType: TextInputType.emailAddress,
                        ),
                        Padding(
                          padding: EdgeInsets.only(
                              left: 0,
                              right: 0,
                              top: 10,
                              bottom: 10
                          ),
                        ),
                        TextField(
                          controller: passwordController,
                          decoration: InputDecoration(
                              hintText: DemoLocalizations.of(context).password,
                              fillColor: Colors.white,
                              filled: true
                          ),
                          cursorColor: Colors.black,
                          keyboardType: TextInputType.text,
                          obscureText: true,
                        ),
                        Padding(
                          padding: EdgeInsets.only(
                            left: 0,
                            right: 0,
                            top: 10,
                            bottom: 10
                          ),
                        ),
                        RaisedButton(
                          color: Colors.orange,
                          child: Text(DemoLocalizations.of(context).enter.toUpperCase()),

                          onPressed:() => verifyLogin(context),

                        )
                      ],
                    ),
                  ),
                )
              ),
            ),
            Expanded(
              flex: 1,
              child: Container()
            )
          ],
        )
      ),
    );
  }

  AssetImage getBackgroundImage(){
    if(Utils.isOrientationPortrait(context)){
      return new AssetImage("assets/images/background_login.webp");
    }
    else{
      return new AssetImage("assets/images/background_login_land.webp");
    }
  }

  double getHeightFactor(){
    if(Utils.isKeyboardShown(context)) {
      if (Utils.isOrientationPortrait(context)) {
        return 0.20;
      }
      else {
        return 0.30;
      }
    }
    else{
      if (Utils.isOrientationPortrait(context)) {
        return 0.30;
      }
      else {
        return 0.60;
      }
    }
  }

  void verifyLogin(BuildContext context){
    if(emailController.text.isNotEmpty && emailController.text.contains('@')){
      startMainTecnico();
    }
    else{
      Utils.showToast(context, DemoLocalizations.of(context).invalid_login, "Fechar");
    }
  }

  void startMainTecnico() {
    setState(() {
        Navigator.pushReplacement(
          context,
          MaterialPageRoute(builder: (context) => MainTecnicoPage()),
        );
    });
  }
}
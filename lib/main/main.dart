import 'package:boiler_flutter/features/login/login_page.dart';
import 'package:boiler_flutter/localizations/localizations.dart';
import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      localizationsDelegates: [
        const DemoLocalizationsDelegate(),
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
      ],
      supportedLocales: [
        const Locale('pt', ''),
        const Locale('en', ''),
      ],
      title: 'Boilerplate',
      theme: ThemeData(
          primarySwatch: Colors.yellow,
          accentColor: Colors.blueAccent
      ),
      home: LoginPage(),
      /*home: Scaffold(
        body: LoginPage(),
      ),*/
    );
  }
}



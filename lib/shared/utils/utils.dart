import 'package:flutter/material.dart';

class Utils{
  static bool isOrientationPortrait(BuildContext context){
    return MediaQuery.of(context).orientation == Orientation.portrait;
  }

  static bool isKeyboardShown(BuildContext context){
    return MediaQuery.of(context).viewInsets.bottom == 0;
  }

  static showToast(BuildContext context, String message, String action) {
    final snackBar = SnackBar(
      content: Text(message),
      action: SnackBarAction(
        label: action,
        onPressed: () {
          // Some code to undo the change!
        },
      ),
    );
    Scaffold.of(context).showSnackBar(snackBar);
  }
}